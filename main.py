import pandas as pd
import fileProcessing as fp
import searchData as sd
import datetime as dt

class main:
   classFileProcessing =fp.fileProcessing()
   classSearchData = sd.searchData()
   
   tempData=classFileProcessing.getFileData('E:\Projects\Python\\','ex161208.log')
   fullData=tempData
   
   filter404Error=classSearchData.searchFrame(fullData,['status'],['404'])   
   filterLinkFrom404Error = classSearchData.searchRequestLinks(filter404Error,'ustem')
   filterLinkFrom404Error['datetime']=pd.to_datetime(filterLinkFrom404Error['date'] + ' ' + filterLinkFrom404Error['time'])
   #print(filterLinkFrom404Error.columns.values)
   filterLinkFrom404Error.drop(filterLinkFrom404Error[['date','time']],axis=1, inplace=True)
   AccessLinks=filterLinkFrom404Error['ustem'].unique()
   tempTimeSlotData=pd.DataFrame(columns=filterLinkFrom404Error.columns.values)
   for link in AccessLinks:
      tempLinkData=filterLinkFrom404Error[filterLinkFrom404Error['ustem'].isin([link])]
      tempTimeSlotData=tempTimeSlotData.append(classSearchData.searchInTimeSpan(tempLinkData, 20))
   print(tempTimeSlotData)
     