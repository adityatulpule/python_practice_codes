import os
import fileProcessing as fp
import sys
import pandas as pd
import numpy

import gc
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import style
from pyspark.sql import SparkSession



def main():
    pass


def extract_fun():


    """     Main Funciton(1) : This functions extracts data according to the given condition   """
    """     Sub-funcitons(2) : It also gives count of flight with countries and builds graphs """

    with open (r'C:\Users\ADITYA\Desktop\data1.txt') as f:

        cols = ['ID','Airport_name','City','Country','IATA','ICAO','Latitude','Longitude','Altitude','Timezone','DST','Tz_database_time_zone','Type','Source']
        sorted_data = pd.read_csv(f,delimiter = ',',names = cols,engine = 'python')



    Altitude_data =(sorted_data.loc[sorted_data['Altitude'] < 100])
    print (Altitude_data.groupby(['Country'])[['ID']].count())#.rename(columns={'ID':'Count'}).count()
    #print Altitude_data.groupby(['Country'])[['ID']].withColumnRenamed('ID','dd')


    Papua_New_Guinea = Altitude_data.loc[Altitude_data['Country']=='Papua New Guinea']
    Greenland_data = Altitude_data.loc[Altitude_data['Country']=='Greenland']
    Iceland_data = Altitude_data.loc[Altitude_data['Country']=='Iceland']
    Canada_data = Altitude_data.loc[Altitude_data['Country']=='Canada']




    count_a = len(Papua_New_Guinea['ID'])
    count_b = len(Greenland_data['ID'])
    count_c = len(Iceland_data['ID'])
    count_d = len(Canada_data['ID'])


    #.groupby(['Country']).count()

    x = [count_a,count_b,count_c,count_d]
    plt.pie(x)
    plt.xlabel('Frequency of Flights (with altitude less than 100)')
    plt.legend(x)
    plt.show()




if __name__ == '__main__':

    #main()
    extract_fun()
