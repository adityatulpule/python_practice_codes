import kivy
from kivy.app import App
from kivy.uix.label import Label


class Kivy_Basic(App):

    """ This program executes " A Hello word program" using kivy with python for android developement """

    def build(self):
        return Label(text = 'Hello Buddy !!!')


if __name__ == '__main__':

    Kivy_Basic().run()
