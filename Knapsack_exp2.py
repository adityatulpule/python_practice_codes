


def initialize_values():

    """     This function initializes all the values that are needed to be passed to calculate the
            maximum Knapsack value       """

    item_value = [25,10,15,40,50]
    item_weight = [3,2,1,4,5]
    Knapsack_capacity = 6
    number_of_items = len(item_value)
    knapSack_function(Knapsack_capacity, item_weight, item_value, number_of_items)






def knapSack_function(Knapsack_capacity, item_weight, item_value, number_of_items):


    """     This function gets the initial values and calculates the best Knapsack value
            that is possible from the available weights within the (capacity = 6)       """

    K = [   [   0 for x in range(Knapsack_capacity+1)  ] for x in range(number_of_items+1)   ]


    for i in range(number_of_items+1):

        for j in range(Knapsack_capacity+1):
            if i==0 or j==0:
				K[i][j] = 0
            elif item_weight[i-1] <= j:
				K[i][j] = max(item_value[i-1] + K[i-1][j-item_weight[i-1]], K[i-1][j])
            else:
				K[i][j] = K[i-1][j]

    print "So the maximum value of Knapsack : ",K[number_of_items][Knapsack_capacity]



if __name__ == '__main__':

    initialize_values()

