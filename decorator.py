#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      aditya
#
# Created:     25/05/2019
# Copyright:   (c) aditya 2019
# Licence:     <your licence>
#------------------------------------------------------------------------------

def decorator(function):

    """     We have used decorator function to demonstrate the use of decorator     """


    def decorator_fun(a,b):
        if b == 0:
            print "Please enter value for b > 0 "
            return
        function(a,b)
    return decorator_fun

@decorator
def div(a,b):

    return(a/b)

#div = decorator(div)
print div(10,0)
