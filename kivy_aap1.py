import kivy
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button




class MyGrid(GridLayout):

    def __init__(self , **kwargs):

        super(MyGrid , self).__init__(**kwargs)
        self.cols = 1

        self.inside = GridLayout() # here inside grabs the values entered in the text boxes
        self.inside.cols = 2

        self.inside.add_widget(Label(text = 'First Name : '))
        self.name = TextInput(multiline=False)
        self.inside.add_widget(self.name)


        self.inside.add_widget(Label(text = 'Last Name : '))
        self.lastname = TextInput(multiline=False)
        self.inside.add_widget(self.lastname)


        self.inside.add_widget(Label(text = 'email : '))
        self.email = TextInput(multiline=False)
        self.inside.add_widget(self.email)

        self.add_widget(self.inside) # add inside with widget


        #Button
        self.submit = Button(text = 'Go' , font_size = 40)
        self.submit.bind(on_press = self.pressed)
        self.inside.add_widget(self.submit)






    def pressed(self , instance):

        name = self.name.text
        lastname = self.lastname.text
        email = self.email.text

        print('Name \n '+name,'\n Last name \n'+lastname,'\n email \n'+email)

        self.name.text = ''
        self.lastname.text = ''
        self.email.text = ''







class Kivy_Basic(App):

    """     This class generates a functional API using python that accepts
    and prints details into another text file        """

    def build(self):

        return MyGrid()






if __name__ == '__main__':

    Kivy_Basic().run()
