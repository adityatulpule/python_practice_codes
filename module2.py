import nmap




##def main():
##    sys.stdout = open("C:/Users/ADITYA/Desktop/test_scan.txt","w")
##    print ("hello")
##    sys.stdout.close()

def nmapScan2():
 nm = nmap.PortScanner()
 nm.scan(hosts='192.168.0.0/24', arguments='-n -sP -PE -PA21,23,80,3389')
 print nm.scanstats()
 for host in nm.all_hosts():
     print('----------------------------------------------------')
     print('Host : %s (%s)' % (host, nm[host].hostname()))
     print('State : %s' % nm[host].state())
     for proto in nm[host].all_protocols():
         print('----------')
         print('Protocol : %s' % proto)

         lport = nm[host][proto].keys()
         lport.sort()
         for port in lport:
             print ('port : %s\tstate : %s' % (port, nm[host][proto][port]['state']))

if __name__ == '__main__':
    nmapScan2()
