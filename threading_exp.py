import threading
import time

def main(a):

    """ This function executes 2 threads together   """

    for i in xrange(10):
        print "thread %d is running \n "%a
        time.sleep(1)
    return

t1 = threading.Thread(target = main, args =(1,))
t2 = threading.Thread(target = main, args =(2,))
t1.start()
t2.start()

for i in xrange(7):

   time.sleep(2)
   print"  thread 1 status : %d    thread 2 status : %d "%(t1.isAlive(),t2.isAlive())


##if __name__ == '__main__':
##    main(a)
