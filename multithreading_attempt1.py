from multiprocessing.dummy import pool as threadpool
import urllib2
import threadpool.

def pool_function():

    urls = ["http://google.com", "http://yahoo.com"]
    pool = threadpool(4)
    results = pool.map(urllib2.urlopen, urls)
    pool.close()
    pool.join()
    print results



if __name__ == '__main__':
    pool_function()
