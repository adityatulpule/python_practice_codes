


class Hash():

    def __init__(self):
        self.size = 5
        self.map = [None] * self.size

    def get_hash(self,key):

        """ This funciton gets ascii value of each character and mods it with hash """

        hash = 0
        for char in str(key):
            hash += ord(char)
        return hash % self.size

    def add(self,key,value):

        """ Function adds new value to the hashtable """

        key_hash = self.get_hash(key)
        key_value = [key,value]

        if self.map[key_hash] is None :
            self.map[key_hash] = list([key_value])
            return True

        else :

            for pair in self.map[key_hash]:

                if pair[0] == key:
                    pair[1] = value

                    return True

            self.map[key_hash].append(key_value)
            return True


    def get(self,key):

        """ Pops the value of the hash that we get using this function """

        key_hash = self.get_hash(key)

        if self.map[key_hash] is not None :

            for pair in self.map[key_hash]:

                if pair[0] == key:

                    return pair[1]


        return None


    def delete(self, key):

        key_hash = self.get_hash(key)

        if self.map[key_hash] is None :
            return False

        for i in range(0,len(self.map[key_hash])):

            if self.map[key_hash][i][0] == key:
                self.map[key_hash].pop(i)
                return True



    def print_function(self):

        print ('\n------- Phone book -----------\n')

        for item in self.map:

            if item is not None:
                print str(item)




Hash_obj = Hash()

Hash_obj.add('BOB','205-222-2222')
Hash_obj.add('Aditya','205-222-9999')
Hash_obj.add('John','205-222-8888')
Hash_obj.add('Salim','205-222-1111')
Hash_obj.add('Bond','205-222-4444')
Hash_obj.print_function()
Hash_obj.delete('BOB')
Hash_obj.print_function()

if Hash_obj.get('John') is None:   # this will check if this record actually exist
    print "No such record found "
else:
    print ('\n John :' +Hash_obj.get('John'))
