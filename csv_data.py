
import csv
import os

def main1():

    """     This function writes values into a new file  """

    with open('C:/Users/aditya/Desktop/samplefile1.csv','w') as csvfile :

        writer = csv.writer(csvfile)
        writer.writerow(['Game','Cricket'])


def main2():

    """     This function appends values into a file     """

    with open('C:/Users/aditya/Desktop/samplefile1.csv','a') as csvfile :

        writer = csv.writer(csvfile)
        writer.writerow(['Game','Cricket'])


def main3():

    """     Check if the file empty or not   """

    if (os.stat("C:/Users/aditya/Desktop/samplefile1.csv").st_size != 0):

        print ('file is not empty')
    else:
        print('file is empty')


if __name__ == '__main__':
    main3()
