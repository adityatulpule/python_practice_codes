from nltk.tokenize import sent_tokenize , word_tokenize , WordPunctTokenizer

#import nltk
#nltk.download('punkt')

input_string = "Aditya is a wonder boy. Thus he can make wonders. It is Aditya's duty to be nice. "

print ("Scentence Seperation :")
print(sent_tokenize(input_string))

print ("Word Seperation :")
print(word_tokenize(input_string))

print ("Word Punct Seperation :")    # This also saperates ('Aditya' ; ' ' ' ; 's') 3 things in a row. 
print(WordPunctTokenizer().tokenize(input_string))
