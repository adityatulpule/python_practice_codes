import pyttsx3     # had to install this on anaconda scripts first and then import that enviornment on visual studio
import datetime
import speech_recognition as sr
import wikipedia
import urllib
from bs4 import BeautifulSoup




engine = pyttsx3.init('sapi5')
voices = engine.getProperty('voices')
#print (voices[0].id)
engine.setProperty('voice',voices[0].id)


def speak(audio):

	engine.say(audio)
	engine.runAndWait()

def Hello_Jarvis():

	hour = (datetime.datetime.now().hour) # time

	if  12 <= hour < 5  :
		speak("Good afternoon Sir ! As always ,its a great pleasure watching you work ! ")

	elif hour < 12 :
		speak("Good morning Sir ! As always ,its a great pleasure watching you work !")

	else :
		speak("Good evening Sir ! As always ,its a great pleasure watching you work !")

	speak(" Jarvis at your service Sir !")

def scraping_1():

      data = urllib.request.urlopen('https://www.yahoo.com/')
      content = data.readlines()
      #print content




      data = []
      for x in content:

        #output = x.strip('\n')
        #print output
        soup = BeautifulSoup(x)
        for link in soup.find_all('span'):
                    #print (link.text).encode('utf-8')
                    data.append((link.text).encode('utf-8'))
      speak (data[12])
      speak (data[19])
      speak (data[24])
      speak (data[29])
      speak (data[33])
      speak (data[37])
      speak (data[43])
      speak (data[46])
      #print data


def Listen():

	#while True :
		r = sr.Recognizer()
		with sr.Microphone() as source :
			print ("Listening .... ")
			r.pause_threshold = 1
			audio = r.listen(source)

	#while True :
		try :
			print("Recognising .... ")
			query = r.recognize_google(audio,language = 'en-in')
			print (f"User said : {query}\n")


		except Exception as e :

			print (e)
			print ("Please say again ! ")
			speak("I couldn't get you Sir , can you please repeat ! ")
			#exit()
			return "None"
		return query






if __name__ == "__main__" :

	#speak(" read between the lines kid !")
	Hello_Jarvis()

	while True :

		query = Listen().lower()

		if 'tell' in query :

			speak("Sure Sir !")

			query = query.replace("tell","")
			results = wikipedia.summary(query,sentences = 2)
			speak("Well , according my Research Sir ")
			speak(results)


		elif 'news' in query:

			speak("Let me check Sir !")
			speak(" Today, Sir  ")
			scraping_1()


		elif 'stop' in query:

			speak("Fine good bye Sir , hope to see you soon !")
			exit(0)
			break


