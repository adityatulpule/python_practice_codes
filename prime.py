import functools




def prime():

    """  This function gives a list of prime and non prime numbers
     in a specified range     """

    n = input("Enter prime range start limit:")
    m = input("Enter prime range end limit :")

    for i in range(int(n),int(m)):
 # this is the list of prime numbers between 2 and 10


        if (i!=2 and i%2 == 0 or i!=3 and i%3 == 0 or i!=5 and i%5 == 0 or i!=7 and i%7 == 0):

            print()
            print(i," Not a prime number ! ")
            print()
        else:

            print(i, " is a prime number  ")





if __name__ == '__main__':

    prime()
