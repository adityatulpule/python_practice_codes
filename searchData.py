# -*- coding: utf-8 -*-
"""
Created on Mon Apr 10 12:25:50 2017

@author: akhil
"""
import pandas as pd
import datetime as dt

class searchData:
#    def search1(self, searchString, searchPattern):        
#        return searchString.str.contains(searchPattern)
    filtered404FinalData=pd.DataFrame()
    
    def search(self, dataFrame, columnName, searchPattern):
        
        index=dataFrame[columnName].str.contains(searchPattern)
        return dataFrame[index]
        

    def searchFrame(self, dataFrame, columnList, patternList):
        count = 0
        for column in columnList:
            index=dataFrame[columnList[count]].str.contains(patternList[count])
            dataFrame=dataFrame[index]
            count+=1
            
        return dataFrame
    
    
    def searchMultipleEntrySameUser(self, dataFrame, columnList, statusCode):
        
        count = 0
        for column in columnList:
            index=dataFrame[columnList[count]].str.contains(statusCode[count])
            filtered302Frame=dataFrame[index]
        finalFrame =pd.DataFrame(columns=dataFrame.columns.values)
        indexCount=0
        for ipAddress in filtered302Frame.itertuples():          
            ipCount=0
            for ipCheck in filtered302Frame.itertuples():
                if(ipCheck[1] == ipAddress[1]):
                    ipCount+=1     
                    
            if(ipCount>1):
                finalFrame=pd.concat([finalFrame, filtered302Frame[indexCount-1:indexCount]], ignore_index=True)
#               
            indexCount+=1
        
        return finalFrame                
    
    def searchRequestLinks(self, dataFrame, columnName):
        f=dataFrame[columnName].groupby(dataFrame[columnName]).size()>1
        f=f[f[0:]==True]
        requestData=f.index.values
        filteredData=dataFrame[dataFrame[columnName].isin(requestData)]
        return filteredData
    
    def searchInTimeSpan(self,frameData, timeBlock):
        temp=pd.DataFrame(columns=frameData.columns.values)
        for timeData in frameData.itertuples():
            startTime=timeData[len(timeData)-1]
            endTime = startTime+dt.timedelta(seconds=timeBlock)    
            d=frameData[(frameData['datetime'] >= startTime) & (frameData['datetime'] <= endTime)]
            rowIndex1=d.index.values
            rowIndex2=temp.index.values
            temp=temp.append(d.loc[list(set(rowIndex1)-set(rowIndex2))])
        return temp
    
    #def search404Error(self, fullData, filtered404, timeInterval):
        #index404 = filtered404.index.values
        #filtered404Frame = pd.DataFrame(columns=fullData.columns.values)
                
        ##print(index404)
        #for rows in filtered404.itertuples():
            #startTime = rows[4]
            #index=rows[0]
            ##startTime=  startTime.str.strip()
            #endTime = dt.datetime.strptime(startTime, '%d/%b/%Y:%I:%M:%S') + dt.timedelta(seconds=10)
            #startTime=dt.datetime.strptime(startTime, '%d/%b/%Y:%I:%M:%S')
            #resourceToCheck=rows[6]
            #count=0
            #for data in fullData[index:]:
                #if(resourceToCheck==data[6]):
                    #pro
           
       