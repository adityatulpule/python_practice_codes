import nmap


def main():
    nm = nmap.PortScanner()
    nm.scan('127.0.0.1', '22-443')
    nm.command_line()
    'nmap -oX - -p 22-443 -sV 127.0.0.1'
    nm.scaninfo()
    {'tcp': {'services': '22-443', 'method': 'connect'}}
    nm.all_hosts()
    ['127.0.0.1']
    nm['127.0.0.1'].hostname()
    'localhost'
    nm['127.0.0.1'].state()
    'up'
    nm['127.0.0.1'].all_protocols()
    ['tcp']
    nm['127.0.0.1']['tcp'].keys()
    [80, 25, 443, 22, 111]
    nm['127.0.0.1'].has_tcp(22)
    True
    nm['127.0.0.1'].has_tcp(23)
    False
    nm['127.0.0.1']['tcp'][22]
    {'state': 'open', 'reason': 'syn-ack', 'name': 'ssh'}
    nm['127.0.0.1'].tcp(22)
    {'state': 'open', 'reason': 'syn-ack', 'name': 'ssh'}
    nm['127.0.0.1']['tcp'][22]['state']
    'open'

    for host in nm.all_hosts():
     print('----------------------------------------------------')
     print('Host : %s (%s)' % (host, nm[host].hostname()))
     print('State : %s' % nm[host].state())
     for proto in nm[host].all_protocols():
         print('----------')
         print('Protocol : %s' % proto)

         lport = nm[host][proto].keys()
         lport.sort()
         for port in lport:
             print ('port : %s\tstate : %s' % (port, nm[host][proto][port]['state']))




    print(nm.csv())



    nm.scan(hosts='192.168.1.0/24', arguments='-n -sP -PE -PA21,23,80,3389')
    hosts_list = [(x, nm[x]['status']['state']) for x in nm.all_hosts()]
    for host, status in hosts_list:
         print('{0}:{1}'.host)




    nma = nmap.PortScannerAsync()
    def callback_result(host, scan_result):
     print '------------------'
     print host, scan_result

    nma.scan(hosts='192.168.1.0/30', arguments='-sP', callback=callback_result)
    while nma.still_scanning():
     print("Waiting >>>")
     nma.wait(2)   # you can do whatever you want but I choose to wait after the end of the scan


    #nm = nmap.PortScannerYield()
    #for progressive_result in nm.scan('127.0.0.1/24', '22-25'):
    # print(progressive\_result)

if __name__ == '__main__':
    main()
