from sklearn import tree
from sklearn import cluster

def guess_fruit():

    # machine learning

    features = [[130,"smooth"],[180,"bumppy"],[150,"bumppy"],[160,"smooth"]]
    lables = ["apple","apple","orange","orange"]
    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(features,lables)
    clf.predict([[130,"smooth"]])


if __name__ == '__main__':

    guess_fruit()
