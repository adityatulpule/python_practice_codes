import numpy as np
from matplotlib import pyplot as plt
from matplotlib import style
import pandas as pd
from pyspark.sql import SparkSession


##def fun1():
##
##    with open ('C:\Users\ADITYA\Desktop\data1.txt') as f:
##        read_data = f.readlines()
##        sorted_data = pd.DataFrame(read_data)
##        print sorted_data
##


def flight_details():

    """     This funciton sorts data in the dataframe format and plots a graph out of it    """

    with open ('C:\Users\ADITYA\Desktop\data1.txt') as f:

        cols = ['ID','Airport_name','City','Country','IATA','ICAO','Latitude','Longitude','Altitude','Timezone','DST','Tz_database_time_zone','Type','Source']
        sorted_data = pd.read_csv(f,delimiter = ',',names = cols,engine = 'python')

    print sorted_data

    x_axis = []
    y_axis = []

    for i in sorted_data['Latitude']:

        x_axis.append(i)

    for i in sorted_data['Longitude']:

        y_axis.append(i)



    style.use('ggplot')

    plt.plot(x_axis,y_axis)
    plt.scatter(x_axis, y_axis, marker = 'o', color='r', zorder=5)

    plt.grid(True,color = 'k')
    plt.title(" Longitude with Latitude ")
    plt.xlabel("x - axis")
    plt.ylabel("y - axis")
    #plt.legend()
    plt.show()


    #plt.bar(x_axis,y_axis,label = 'Bar type graph')


##
##    fig = plt.figure(2,3)
##    ax = fig.add_subplot(111)
##    ax.set_xscale('log')




if __name__ == '__main__':

    #fun1()
    flight_details()
