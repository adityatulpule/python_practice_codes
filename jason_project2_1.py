import pandas as pd
import ast
import json
import numpy as np
import matplotlib.pyplot as plt



def prase_data_file():

    df = pd.read_csv(r'C:\Users\ADITYA\Desktop\UAB\Notes\Sem3\Big Data\League_of_Legends\data.txt',sep = '",',header = None,engine = 'python')

    for i in df[0]:
        data2 = ast.literal_eval(i)

        for i in data2 :
            print (i)

        print
        print ('Parsing for type attribute :')

        for i in data2 :
            print (i['type'])




def json_parsing2():

    """ This function pops data for riftwalk time and region """

    json_file = open(r'C:\Users\ADITYA\Desktop\UAB\Notes\Sem3\Big Data\League_of_Legends\riftwalk-sample.json').readline()
    data = json.loads(json_file)
    #print (data)
    for str in data :
        print str
    print
    print
    print (data['champion_ids'])

    #print (data['players'])

    #for i in data['players']:
     #   print (i['name'])

    region = data['region']
    print data['summoner_names']
    playtime = (data['length']/60),'mins'




def europe_data():

    """ This function does the exact parsing for the player names and their perticular attributes """


    data_list1 = []
    data_list2 = []
    json_file = open(r'C:\Users\ADITYA\Desktop\UAB\Notes\Sem3\Big Data\League_of_Legends\JSON_EUW2.json').readline()
    data = json.loads(json_file)

    #print (data['players'])

    for single_dict in data['players']:
        #print single_dict['champion_name']
        #print single_dict['scores']#['kills']
        #for i in single_dict:
         #   print i
        player_kills_data = { single_dict['champion_name'] :single_dict['scores']['kills']}
        print player_kills_data
        data_list1.append(single_dict['champion_name'])
        data_list2.append(single_dict['scores']['kills'])
    #print data_list1

    x = data_list1
    y = data_list2
    ind = np.arange(len(x))
    plt.bar(ind,y)
    plt.xticks(ind,x)
    plt.xlabel(' CHAMPION PLAYER NAMES IN EUROPE')
    plt.ylabel(' KILLS PER PLAYER ')
    plt.show()




def Korean_data():

    """     This function parses Korean data files for data about the players """

    data_list1 = []
    data_list2 = []
    json_file = open(r'C:\Users\ADITYA\Desktop\UAB\Notes\Sem3\Big Data\League_of_Legends\JSON_kr.json').readline()
    json.dumps(json_file)
    data = json.loads(json_file)

    #print data
    #print (data['players'])

    for single_dict in data['players']:
        #print single_dict['champion_name']
        #print single_dict['scores']#['kills']
        #for i in single_dict:
         #   print i
        player_kills_data = { single_dict['champion_name'] :single_dict['scores']['kills']}
        print player_kills_data
        data_list1.append(single_dict['champion_name'])
        data_list2.append(single_dict['scores']['kills'])
    #print data_list1

    x = data_list1
    y = data_list2
    ind = np.arange(len(x))
    plt.bar(ind,y)
    plt.xticks(ind,x)
    plt.xlabel(' CHAMPION PLAYER NAMES IN KOREA ')
    plt.ylabel(' KILLS PER PLAYER ')
    plt.show()




def North_America():


    """     This function parses North American data files for data about the players """

    data_list1 = []
    data_list2 = []
    json_file = open(r'C:\Users\ADITYA\Desktop\UAB\Notes\Sem3\Big Data\League_of_Legends\JSON_na.json').readline()
    json.dumps(json_file)
    data = json.loads(json_file)

    #print data
    #print (data['players'])

    for single_dict in data['players']:
        #print single_dict['champion_name']
        #print single_dict['scores']#['kills']
        #for i in single_dict:
         #   print i
        player_kills_data = { single_dict['champion_name'] :single_dict['scores']['kills']}
        print player_kills_data
        data_list1.append(single_dict['champion_name'])
        data_list2.append(single_dict['scores']['kills'])
    #print data_list1

    x = data_list1
    y = data_list2
    ind = np.arange(len(x))
    plt.bar(ind,y)
    plt.xticks(ind,x)
    plt.xlabel(' CHAMPION PLAYER NAMES IN NORTH AMERICA ')
    plt.ylabel(' KILLS PER PLAYER ')
    plt.show()

def NA_playerDamaged():


    data_list1 = []
    data_list2 = []
    json_file = open(r'C:\Users\ADITYA\Desktop\UAB\Notes\Sem3\Big Data\League_of_Legends\JSON_na.json').readline()
    json.dumps(json_file)
    data = json.loads(json_file)

    #print data
    #print (data['players'])

    for single_dict in data['players']:
        #print single_dict['champion_name']
        #print single_dict['time_played']
        #print single_dict['scores']
        dm = (int(single_dict['total_damage_received'])/1000)
        print dm
        print
        #for i in single_dict:
           # print i

        #player_kills_data = { single_dict['champion_name'] :single_dict}
        #print player_kills_data
        data_list1.append(single_dict['champion_name'])
        data_list2.append(dm)


    x = data_list1
    y = data_list2
    ind = np.arange(len(x))
    plt.bar(ind,y)
    plt.xticks(ind,x)
    plt.xlabel(' CHAMPION PLAYER NAMES IN EUROPE')
    plt.ylabel(' DAMAGE RECEIVED PER PLAYER ')
    plt.show()




def time_played():
    pass







if __name__ == '__main__':

    #prase_data_file()
    #json_parsing2()
    #europe_data()
    #Korean_data()
    North_America()
    NA_playerDamaged()