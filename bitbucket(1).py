

'''                           CODE KATA (21) SIMPLE LIST      '''


def List_operations():

    """ basic string operations """

    List = ['aaa','bbb','ccc']
    list1 = ['physics', 'chemistry', 1997, 2000];
    list2 = [1, 2, 3, 4, 5, 6, 7 ];

    print 'To display a specific range in a list :'
    print list2[1:4]
    print'------------------------------------------------------------'

    print ' To give boolean output if element present in list '
    print 3 in [1,2,3]
    print'------------------------------------------------------------'

    print' To print string multiple times '
    print ' hello ' * 10
    print'------------------------------------------------------------'

    print' To print all elements in a list'
    for x in list1:
        print x
    print'------------------------------------------------------------'

    print' To keep count of perticular elements in a list'
    print list1.count(1997)
    print'------------------------------------------------------------'

    print ' To put ggg at specified location'
    list1.insert(2,'ggg')
    print list1
    print'------------------------------------------------------------'

    print ' To append passed variable to the list '
    list1.append(4)
    print list1
    print '------------------------------------------------'
    print


def assertion_tempetarure(temperature):

    """ This function returns assertion Error for values less than zero """

    print KelvinToFahrenheit(temperature)
    print'------------------------------'
    print

def KelvinToFahrenheit(Temperature):

   print "Weather in degrees F"
   print  Temperature
   assert (Temperature >= 0),"Colder than absolute zero!"



if __name__ == '__main__':

    List_operations()
    assertion_tempetarure(27) # if negative temperature is passed  assertion error pops up !
