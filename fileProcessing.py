
import pandas as pd
from pandas.compat import StringIO
import io
import re

class fileProcessing:

    def getFileData(self, fileLocation, fileName):

        with open(str(fileLocation)+str(fileName)) as f:

            f=f.readlines()
            important =""
            data=pd.DataFrame(columns=None)
            columnFlag=True
            columnList=list()

            for line in f:
                if not (re.match('^#', line)):
                    line=re.split(' ', line.strip())
                    tempList=list()
                    tempDict={}
                    index=0

                    for item in line:
                        tempDict[columnList[index]]=line[index]
                        index+=1

                    data=data.append(tempDict, ignore_index=True)

                elif (re.match('^#Fields', line)):
                    if (columnFlag==True):
                        m=re.split(' ',line)

                        for item in m:
                            if not(re.match('^#|\n',item)):
                                columnList.append(re.sub("\w\w-","",item))

                        for cName in columnList:
                            data[cName]=''

                        columnFlag=False
            return data

if __name__ == '__main__':

    main()
