




def initialize_values():


    val = [60, 100, 120]
    wt = [10, 20, 30]
    W = 50
    n = len(val)
    Knapsack_function(W, wt, val, n)



def Knapsack_function(W, wt, val, n):
    """lll"""

    K = [[0 for x in range(W+1)] for x in range(n+1)]


    for i in range(n+1):
        for w in range(W+1):
            if i==0 or w==0:
                K[i][w] = 0
            elif wt[i-1] <= w:
                K[i][w] = max(val[i-1] + K[i-1][w-wt[i-1]],  K[i-1][w])
            else:
                K[i][w] = K[i-1][w]

    print K[n][W]



if __name__ == '__main__':

    initialize_values()