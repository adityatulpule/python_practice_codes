%matplotlib inline
import matplotlib.pyplot as plt 
from pylab import rcParams
import seaborn as sb
import pandas as pd
import sklearn
from sklearn.cluster import DBSCAN 
from collections import Counter 

rcParams['figure.figsize'] = 5,4
sb.set_style('whitegrid')


######

file_name = 'A:\Sem_4\Data Mining\mini_project_dataset.xlsx'

xl_file = pd.ExcelFile(file_name)

dfs = {sheet_name: xl_file.parse(sheet_name) 
          for sheet_name in xl_file.sheet_names}


data2 = (list(dfs.values())[0])
data = pd.DataFrame(data2)

#####

model = DBSCAN(eps = 2 ,min_samples = 2).fit(data)
#print (model)

#####

outliers_df = pd.DataFrame(data)

counter_val = (Counter(model.labels_))
print (counter_val)

#print (outliers_df[model.labels_ == -1])

print  (counter_val.values())
print  ((counter_val.keys()))
print ()
print (outliers_df[model.labels_ == -1])
#####
x = ((counter_val.keys()))
y = (counter_val.values())
x1 =  (data2['Price'])
y1 =  (data2['Goods and Luggage'])



#bar_label = ['outliers','0','1','2','3']
#plt.xticks(x,bar_label)
plt.bar(x,y)
plt.title(" SALE PER DAY ")
plt.xlabel('point distribution ')
plt.ylabel('number of Days ')

plt.show()


plt.scatter(x,y,label = ' Goods / day ')
plt.scatter(x1,y1,label = ' Price for goods / day ')
plt.show()







ax = plt.subplot2grid((6,1),(1,0),rowspan = 1 , colspan= 1)
ax.plot(x1,y1)
plt.show()


