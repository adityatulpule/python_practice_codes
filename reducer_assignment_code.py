

from operator import itemgetter
import sys


def reducer():

    # trial code for reducer

    word_in_file = None
    current_count = 0
    word = None

    for line in sys.stdin:

        line = line.strip()
        word, count = line.split('\t',1)

        try:
            count = int(count)
        except ValueError:
            continue

        if word_in_file == word :
            current_count += count
        else:
            if word_in_file:
                print"%s\t%s",(word_in_file,current_count)
            current_count = count
            word_in_file = word

    if word_in_file == word:
        print"%s\t%s",(word_in_file,current_count)



if __name__ == '__main__':
    reducer()
