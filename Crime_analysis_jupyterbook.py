

import xlrd
import numpy as np
from ipfn import *
%matplotlib inline
import matplotlib.pyplot as plt



def main():

"""     This code aggregates the crime data using itterative proportional fitting. It extracts data from a xls file to find the data regarding crimes committed from 1997 to 2016   """
"""     This code was originally typed and executed using jupyter notebook """

workbook = xlrd.open_workbook('A:\Sem_4\Foundation of Data Science\Final_Project\project_data\\table-1.xls')
#print(workbook)
worksheet = workbook.sheet_by_index(0)

total_rows = worksheet.nrows
total_cols = worksheet.ncols

table = []
record = []






#plt.show()

for x in range(4,24):
    for y in range(total_cols):
        record.append(worksheet.cell(x,y).value)
    table.append(record)
    record = []
    x += 1

#print (table[0][0])   # only year

#print (table)

year = []
crime1 = []
crime2 = []
crime3 = []
crime4 = []
crime5 = []
crime6 = []
crime7 = []
crime8 = []
crime9 = []

for i in table:
    #print (i[0],'\t',i[5],'\t',i[6])

    year.append(str(i[0]))
    crime1.append(str(i[3]))
    crime2.append(str(i[5]))
    crime3.append(str(i[7]))
    crime4.append(str(i[9]))
    crime5.append(str(i[11]))
    crime6.append(str(i[13]))
    crime7.append(str(i[15]))
    crime8.append(str(i[17]))
    crime9.append(str(i[19]))
    #print(i[9])


plt.rcParams['figure.figsize'] = (35,35)
plt.plot(year,crime1,label = ' Violent crime rate')
plt.scatter(year,crime1)
plt.plot(year,crime2,label = 'Murder and non - egligent manslaughter rate')
plt.scatter(year,crime2)
plt.plot(year,crime3,label = 'Rape (legacy defination) rate')
plt.scatter(year,crime3)
plt.plot(year,crime4,label = 'Robbery')
plt.scatter(year,crime4)
plt.plot(year,crime5,label = 'Aggravated assault rate')
plt.scatter(year,crime5)
plt.plot(year,crime6,label = 'Property Crime rate')
plt.scatter(year,crime6)
plt.plot(year,crime7,label = 'Burglary rate')
plt.scatter(year,crime7)
plt.plot(year,crime8,label = 'Larceny theft rate')
plt.scatter(year,crime8)
plt.plot(year,crime9,label = 'Motor vehicle theft rate')
plt.scatter(year,crime9)
plt.legend(fontsize="large")
plt.grid()
plt.title(' INDIVIDUAL CRIME DATA')
plt.show()















workbook = xlrd.open_workbook('A:\Sem_4\Foundation of Data Science\Final_Project\project_data\\table-1.xls')
#print(workbook)
worksheet = workbook.sheet_by_index(0)

total_rows = worksheet.nrows
total_cols = worksheet.ncols

table = []
record = []

for x in range(4,24):
    for y in range(total_cols):
        record.append(worksheet.cell(x,y).value)
    table.append(record)
    record = []
    x += 1

#print (table[0][0])   # only year

#print (table)

year2 = []
crime1 = []
crime2 = []
crime3 = []
crime4 = []
crime5 = []
crime6 = []
crime7 = []
crime8 = []
crime9 = []

for i in table:
    #print (i[0],'\t',i[5],'\t',i[6])

    year2.append(str(i[0]))
    crime1.append(int(i[3]))
    crime2.append(int(i[5]))
    crime3.append(str(i[7]))
    crime4.append(int(i[9]))
    crime5.append(int(i[11]))
    crime6.append(int(i[13]))
    crime7.append(int(i[15]))
    crime8.append(int(i[17]))
    crime9.append(int(i[19]))
    #print(i[9])




seed_tensor = np.array([crime1,crime2,crime3,crime4,crime5,crime6,crime7,crime8,crime9])
print()
print('Column values to be aggregated using IPF :')
print ()
print(seed_tensor)

margins_r = np.array([20,30,35,20,30,35,20,30,35])
#margins_r

margins_c = np.array([35,40,25,35,40,25,35,40,25,35,40,25,35,40,25,35,40,25,35,40,35,40,25,35,40,25,35,40,25,35,35,40,25,35,40,25,35,40,25,35,35,40,25,35,40,25,35,40,25,35,35,40,25,35,40,25,35,40,25,35,35,40,25,35,40,25,35,40,25,35,35,40,25,35,40,25,35,40,25,35,35,40,25,35,40,25,35,40,25,35])


aggregates = [margins_r, margins_c]
dimensions = [[0], [1]]

in2 = seed_tensor.copy()
print(in2)
print()

IPF = ipfn.ipfn(in2.astype('float'), aggregates, dimensions) # see what happens if we do not have .astype('float')?

out = IPF.iteration()
#print(in2)
print()
print (' Following iteration for ipf : ')
print('----------------------------------')
print()
print(out)
for i in out:
    print ()
    data = list(i)

print ((data))
print (year2)
print ()
print(' Data Calculated after Iterative Proportional Fitting : ')

plt.rcParams['figure.figsize']= (15,6)
plt.bar(year2,data,width = 0.9)

plt.title(" Crimes PER Year ")
plt.xlabel('Year ')
plt.ylabel('Crime Rate / Year')

plt.show()

if __name__ == '__main__':
    main()
