
import openpyxl
import os

def main():

    """ This funciton can make changes to the excel file and handles the data """


    os.chdir('D:\python_programs')      # switch to the current directory
    wb = openpyxl.load_workbook('company_data.xlsx')

    #print wb.get_sheet_names()[0]
    sheet = wb.get_sheet_by_name(name ='Sheet1')
    #print sheet

    sheet['C1'].value = 88              # updates values to the specified locaiton
    wb.save('company_data.xlsx')        # saves changes to the actual file

    for i in range(1,3) :

        print sheet.cell(row = i, column = 1).value



if __name__ == '__main__':
    main()
