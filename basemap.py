from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt


def Basemap_1():

	"""	This function draws a world map using the basemap liabrary	"""

	m = Basemap(projection = 'mill' )

	m.drawcoastlines()

	plt.title("world map")
	plt.show()

if __name__ == '__main__' :
	
	Basemap_1()
