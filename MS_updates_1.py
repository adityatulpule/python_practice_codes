
import os
import time
import datetime
import timeit

def Main():


    Process = os.popen("powershell $Ses = New-Object -Com 'Microsoft.Update.Session'; $Sea = $Ses.CreateUpdateSearcher(); $Sea.QueryHistory(0, 200)");
    time.sleep(5);
    RawData = Process.read();


    RawData = RawData.replace(" ", "");


    UpdateList = RawData.split("\n\n");


    UpdateList = [Item for Item in UpdateList if Item != ""];


    UpdateList = [Item.split("\n") for Item in UpdateList];


    NewUpdateList = [];
    for Update in UpdateList:
        NewUpdate = ["Date:" + Item.replace(":", "-")[5:] for Item in Update if "Date" in Item];
        NewUpdate += [Item for Item in Update if "Date" not in Item];
        NewUpdateList.append(NewUpdate);
        UpdateList = NewUpdateList;


    NewUpdateList = [];
    for Update in UpdateList:
        UpdateDict = {};

        for Item in Update:
            try:
                Key, Value = Item.split(":");
            except ValueError:
                Value = None;
                pass;

            #Fix date format
            if(Key == "Date"):
                Value = Value[:10] + " " + Value[10:];

            UpdateDict[Key] = Value;

        NewUpdateList.append(UpdateDict);
    UpdateList = NewUpdateList;

    #Get latest date of updates
    DateList = [Item["Date"].split(" ")[0] for Item in UpdateList];
    DateList = sorted(DateList, key=lambda x: timeit.time.clock(), reverse=True);
    LatestDate = DateList[0];

    #Filter update list on latest date
    UpdateList = [Item for Item in UpdateList if Item["Date"].split(" ")[0] == LatestDate];

    print("Last batch of '{0}' items installed on '{1}'".format(len(UpdateList), LatestDate))

    for Update in UpdateList:
        print("Time:  %s" % Update["Date"]);
        print("Title: %s" % Update["Title"])

if(__name__ == "__main__"):
    Main()