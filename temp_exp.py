#-------------------------------------------------------------------------------
# Name: Dictionary experiments
# Purpose: experiments
#
# Author:      ADITYA
#
# Created:     05-08-2017
# Copyright:   (c) ADITYA 2017
# Licence:     ART Publications
#-------------------------------------------------------------------------------


import csv
from filelock import FileLock


def match_value_type():

    """       This function verifies the datatype wether it is a dictionary or not and then prints its values       """

    expdict = {"ady":[1,{"mulga":"motha"}],"baba":2,"aai":3}

    for i in expdict['ady']:
        if type(i) == dict :
            print i['mulga']



def deep_decode_dict():

    """     This function stores required values from the dictionary into the csv file      """

    dict_3 = {u'_total': 2, u'values': [{u'updateKey': u'UPDATE-c13308290-6270853550922924032', u'isCommentable': True, u'updateContent': {u'company': {u'id': 13308290, u'name': u'Extreme Mechanics'}, u'companyStatusUpdate': {u'share': {u'comment': u'Best engine product at lowest price !', u'source': {u'serviceProvider': {u'name': u'LINKEDIN'}, u'serviceProviderShareId': u's6270853550935502848'}, u'id': u's6270853550935502848', u'visibility': {u'code': u'anyone'}, u'timestamp': 1495087993375L}}}, u'isLiked': False, u'timestamp': 1495087993375L, u'numLikes': 1, u'isLikable': True, u'updateType': u'CMPY', u'updateComments': {u'_total': 0}, u'likes': {u'_total': 1, u'values': [{}]}}, {u'updateKey': u'UPDATE-c13308290-6268678386844037120', u'isCommentable': True, u'updateContent': {u'company': {u'id': 13308290, u'name': u'Extreme Mechanics'}, u'companyStatusUpdate': {u'share': {u'comment': u'Hot wheel business never die \n                                                                - Aditya', u'source': {u'serviceProvider': {u'name': u'LINKEDIN'}, u'serviceProviderShareId': u's6268678386856628224'}, u'id': u's6268678386856628224', u'visibility': {u'code': u'anyone'}, u'timestamp': 1494569393844L}}}, u'isLiked': False, u'timestamp': 1494569393844L, u'numLikes': 1, u'isLikable': True, u'updateType': u'CMPY', u'updateComments': {u'_total': 0}, u'likes': {u'_total': 1, u'values': [{}]}}]}

    column_names = ['name_of_company','ID','Comment']

    with open('C:/Users/ADITYA/Desktop/adityadict.csv', 'w') as csvfile :

        writer = csv.DictWriter(csvfile,column_names)
        writer.writeheader()

        for i in dict_3['values']:
            new_dict = {'name_of_company':i['updateContent']['company']['name'],'ID':i['updateContent']['company']['id']}
            writer.writerow(new_dict)


def file_locker():

    with FileLock('C:\Users\ADITYA\Desktop\locker_exp'):
        print ("lock acquired !")



if __name__ == '__main__':

    #match_value_type()
    #deep_decode_dict()
    file_locker()